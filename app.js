var AngularApp = angular.module('AngularApp', ['ui.router']);

AngularApp.config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider) {

    $stateProvider
        // HOME STATES AND NESTED VIEWS ========================================

        .state('home', {
            url: '/home',
            templateUrl: 'app/templates/home.html',

        })
        .state('home.Table1', {
            url: '/Table1',
            templateUrl: 'app/templates/Table1.html',
            controller: 'Table1Ctrl'
            })

        .state('home.Table2', {
            url: '/Table2',
            templateUrl: 'app/templates/Table2.html',
            controller: 'Table2Ctrl'
        })
        .state('home.Table3', {
            url: '/Table3',
            templateUrl: 'app/templates/Table3.html',
            controller: 'Table3Ctrl'
        })
  $urlRouterProvider.otherwise('/home');
}]);

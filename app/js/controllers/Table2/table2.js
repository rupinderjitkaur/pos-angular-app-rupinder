AngularApp.controller('Table2Ctrl', function ($scope) {
  $scope.showMe = false;
  $scope.val = true;
  $scope.myFunc = function() {
    $scope.showMe = !$scope.showMe;
    $scope.showsubmenu = function (submenu) {
      switch(submenu){
        case 'drinks':
          $scope.val = true;
          $("#myTab").fadeOut(500, function() {
    $(".sub_category").fadeIn(500, function() {
    });
});
          $scope.drinks = [{
              id: 0,
              name: "Still Water",
              price: "10",
          },
          {
              id: 1,
              name: "Tea",
              price: "10",
          },
          {
              id: 2,
              name: "Hot Chocolate",
              price: "10",
          }];
          break;
        case 'food':
          $scope.val = false;
          $("#myTab").fadeOut(500, function() {
    $(".sub_category").fadeIn(500, function() {
    });
});
          $scope.foods = [{
              id: 3,
              name: "maggie",
              price: "20",
          },
          {
              id: 4,
              name: "Pasta",
              price: "30",
          },
          {
              id: 5,
              name: "Pizza",
              price: "90",
          }];
          break;
      }
  }
  $scope.mycat = function () {
    $(".sub_category").fadeOut(500, function() {
      $("#myTab").fadeIn(500, function() {
      });
    });
  }
  $scope.saved = localStorage.getItem('order1');
  $scope.order1 = (localStorage.getItem('order1')!==null) ? JSON.parse($scope.saved) : [];
  localStorage.setItem('order1', JSON.stringify($scope.order1));
    console.log(localStorage.getItem('order1'));
    //$scope.order = [];
    //$scope.new = {};
    $scope.totOrders = 0;


    $scope.getDate = function () {
        var today = new Date();
        var mm = today.getMonth() + 1;
        var dd = today.getDate();
        var yyyy = today.getFullYear();

        var date = dd + "/" + mm + "/" + yyyy

        return date
    };

    $scope.addToOrder = function (item, qty) {
        var flag = 0;
        if ($scope.order1.length > 0) {
            for (var i = 0; i < $scope.order1.length; i++) {
                if (item.id === $scope.order1[i].id) {
                    item.qty += qty;
                    localStorage.setItem('order', JSON.stringify($scope.order1));

                    flag = 1;
                    break;
                }
            }
            if (flag === 0) {
                item.qty = 1;
            }
            if (item.qty < 2) {
                $scope.order1.push(item);
                $scope.todoText = '';
                localStorage.setItem('order1', JSON.stringify($scope.order1));
            }
        } else {
            item.qty = qty;
            $scope.order1.push(item);
            $scope.todoText = '';
            localStorage.setItem('order1', JSON.stringify($scope.order1));

        }
    };

    $scope.removeOneEntity = function (item) {
        for (var i = 0; i < $scope.order1.length; i++) {
            if (item.id === $scope.order1[i].id) {
                item.qty -= 1;
                if (item.qty === 0) {
                    $scope.order1.splice(i, 1);
                    localStorage.setItem('order1', JSON.stringify($scope.order1));

                }
            }
        }
    };

    $scope.getTotal = function () {
      console.log($scope.order1);
        var tot = 0;
        for (var i = 0; i < $scope.order1.length; i++) {
            tot += ($scope.order1[i].price * $scope.order1[i].qty)
        }
        return tot;
    };

    $scope.clearOrder = function () {
        $scope.order1 = [];
    };

    $scope.checkout = function (index) {
        $scope.table2_pay = ($scope.getDate() + " - Order Number: " + ($scope.totOrders+1) + "\n\nOrder amount: $" + $scope.getTotal().toFixed(2) + "\n\nPayment received. Thanks.");
        $scope.order1 = [];
        localStorage.setItem('order2', JSON.stringify($scope.order2));
        $scope.totOrders += 1;
    };
}

});
